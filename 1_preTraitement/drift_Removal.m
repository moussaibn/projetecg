function [ outputSignal ] = drift_Removal( inputSignal)
%DRIFT_REMOVAL Summary of this function goes here

% %  Detailed explanation goes here
% 
%     outputSignal=zeros(1,length(inputSignal));
%     outputSignal(1)=inputSignal(2)-inputSignal(1);
%     for i=2:length(inputSignal)
%        outputSignal(i)=inputSignal(i)-inputSignal(i-1); 
%     end



%% Premi�re M�thode

 xd=  medfilt1(inputSignal,20);
 outputSignal = inputSignal - xd;

   %% Deuxi�me m�thode
%  
% trend_est = fnval(csaps(t,inputSignal,0.2),t);
% outputSignal = inputSignal-trend_est;
%     

%% Troisi�me M�thode
% [p,s,mu] = polyfit((1:numel(inputSignal))',inputSignal,10);
% f_y = polyval(p,(1:numel(inputSignal))',[],mu);
% 
% outputSignal = inputSignal - f_y;    
end

