function [ output ] = smoothing( input )
%SMOOTHING Summary of this function goes here
%   Detailed explanation goes here

%% Premi�re m�thode : Filtre moyenneur avec FILTER
%     B = (1/7)*ones(1,7);
%    output = filter(B,1,input);
   
   
%% Deuxi�me m�thode :FILTFILT

%      d = designfilt('lowpassiir', ...
%     'FilterOrder',12, ...
%     'HalfPowerFrequency',0.15,'DesignMethod','butter');

% d = designfilt('lowpassfir', ...
% 'PassbandFrequency',0.15,'StopbandFrequency',0.2, ...
% 'PassbandRipple',1,'StopbandAttenuation',60, ...
% 'DesignMethod','equiripple');

%  output = filtfilt(d,input);

    %% Trois�me m�thode
%  N=7;
%  L=length(input);
% M=floor(N/2);
% m=zeros(1,L);
% for i=1:M
%    %m(i)=mean(input(1:i));
%     m(i)=0;
% end
% for i=1+M:L-M-1
%     m(i)=mean(input(i-M:i+M));
% end
% for i=L-M:L
%     m(i)=mean(input(i-M:L));
% end
%output =m;


%% Troisi�me m�thode : FILTRE
output = sgolayfilt(input,7,21);


end
