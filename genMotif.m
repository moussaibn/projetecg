clear all
clc
close all
addpath('1_preTraitement','2_traitement','3_frequence');

[t,x,y] = importfile('data/p1_AS3ExportDataECG.csv', 1,100000);
x=drift_Removal(x);
x=smoothing(x);
motif_1=x(23795:23844);

[t,x,y] = importfile('data/p2_AS3ExportDataECG.csv', 1,100000);
x=drift_Removal(x);
x=smoothing(x);
motif_2=x(23795:23844);

[t,x,y] = importfile('data/p3_AS3ExportDataECG.csv', 1,100000);
x=drift_Removal(x);
x=smoothing(x);
motif_3=x(26920:27010);

[t,x,y] = importfile('data/p4_AS3ExportDataECG.csv', 1,100000);
x=drift_Removal(x);
x=smoothing(x);
motif_4=x(26900:26980);

[t,x,y] = importfile('data/p_5AS3ExportDataECG.csv', 1,100000);
x=drift_Removal(x);
x=smoothing(x);
motif_5=x(27030:27110);