clc
close all
addpath('1_preTraitement','2_traitement','3_frequence');

rayon = 50;
[t,x,y] = importfile('data/p1_AS3ExportDataECG.csv', 1, 10000);

xd=drift_Removal(x);

x1=smoothing(xd);

locs_Rwave= r_waves(x1);
locs_Swave= s_waves(x1);
locs_Qwave= q_waves(x1);

plot (x1)
hold on
plot(locs_Rwave,x1(locs_Rwave),'rv','MarkerFaceColor','r')
hold on
plot(locs_Swave,x1(locs_Swave),'rs','MarkerFaceColor','b')
hold on
plot(locs_Qwave,x1(locs_Qwave),'rs','MarkerFaceColor','g');

xlabel('Samples');
ylabel('Voltage (mV)');
legend('ECG','R-Waves','S-waves','Q-Waves');
grid on;



% 
% x1= correl(x1);
%  x1=seuillage(x1);
% x1=parasite(x1,10);
% % 
%  f=freq(x1);
% % 
% plot(t,xd)
%  hold on
%  plot(t,x1,'r');


