clear all
clc
close all
addpath('1_preTraitement','2_traitement','3_frequence');

[t,x,y] = importfile('data/p1_AS3ExportDataECG.csv', 1, 50000);
figure(1);
hold on
%% Drift removal
drift=zeros(1,length(x));

drift(1)=x(2)-x(1);
for i=2:length(x)
    drift(i)=x(i)-x(i-1);
end
x=drift;
N=7;
L=length(x);
M=floor(N/2);
m=zeros(1,L);
% for i=1:M
%    %m(i)=mean(x(1:i));
%     m(i)=0;
% end
% for i=1+M:L-M-1
%     m(i)=mean(x(i-M:i+M));
% end
% for i=L-M:L
%     m(i)=mean(x(i-M:L));
% end
B = (1/7)*ones(7,1);
m = filter(B,1,x);
%/m=movmean(x,7);
m=m';
%%t=t(numel(m),1);
plot(t,m,'b');

v_qrs=m(4237:4273);
[p,ip]=max(v_qrs);
v=v_qrs/p;
L3=length(v_qrs);
for i=1:L-L3-1
    v2=m(i:i+L3-1);
    [p2,ip]=max(v2);
    v2=v2/p2;
    co=corrcoef(v,v2);
    M(i)=min(min(co));
end


%% Seuillage
M(M==1 | M< 0.9)=0;
M(M>= 0.9)=1;
i=1;

for k=1:length(M)
    if (M(k)==1)
        M(k-4:k-1)=0;
        
    end
end
%% Representaions
pics=find(M==1);
T=zeros(1,length(pics));
freq=zeros(1,length(pics));
for i=2:length(T)
    T(i)=pics(i)-pics(i-1);
end
T(1)=pics(1);
t_=t(1:length(M));
for i=1:length(freq)
    freq(i)=T(i);
end
hold on
plot(t_,M,'r');
grid
%legend('origine','filtre ma');
figure(2)
grid
hold on
plot(freq);

