function [ pattern ] = getPattern( filename, ECG )
%GETPATTERN Summary of this function goes here
%   Detailed explanation goes here
load('EcgMotifs.mat');
switch filename
    case 'data/p1_AS3ExportDataECG.csv'
        pattern=motif_1;
    case 'data/p2_AS3ExportDataECG.csv' 
        pattern=motif_2;
    case 'data/p3_AS3ExportDataECG.csv'
        pattern=motif_3;
    case 'data/p4_AS3ExportDataECG.csv'
        pattern=motif_4;
    case 'data/p_5AS3ExportDataECG.csv'
        pattern=motif_5; 
        
end
