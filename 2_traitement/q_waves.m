function [ output ] = q_waves( input )
%Q_WAVES Summary of this function goes here
%   Detailed explanation goes here
inputInv = - input;

[~,min_locs] = findpeaks(inputInv,'MinPeakDistance',40);
locs_Qwave = min_locs(input(min_locs)>-0.4 & input(min_locs)<-0.3);

output = locs_Qwave;
end

