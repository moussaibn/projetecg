function [ output] = seuillage( input )
%SEUILLAGE Summary of this function goes here
%   Detailed explanation goes here

M=input;
M(M<0.96)=0;

k=1;
while(k<length(M))
    j=0;
    while(M(k)~=0 && k<length(M))
        M(k)=0;
        k=k+1;
        j=j+1;
    end
    if(j~=0)
        M(floor((k+k-j)/2))=1;
    end
    k=k+1;
end
output =M;
