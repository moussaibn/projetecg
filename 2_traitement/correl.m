function [ output] = correl( input,motif )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
L=length(input);
output=zeros(1,length(input));

[p]=max(motif);
v=motif/p;
L3=length(motif);
    for i=1:L-L3-1
        v2=input(i:i+L3-1);
        [p2]=max(v2);
        v2=v2/p2;
        co=corrcoef(v,v2);
        output(i)=min(min(co));
    end
end

