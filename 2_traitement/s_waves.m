function [ output] = s_waves( input )
%R_WAVES Summary of this function goes here
%   Detailed explanation goes here
input = - input;
[~,locs_Swave] = findpeaks(input,'MinPeakHeight',0.6,...
                                    'MinPeakDistance',200);
      output = locs_Swave;
end

