function [ output] = r_waves( input )
%R_WAVES Summary of this function goes here
%   Detailed explanation goes here
[~,locs_Rwave] = findpeaks(input,'MinPeakHeight',2.5,...
                                    'MinPeakDistance',200);
      output = locs_Rwave;

end

