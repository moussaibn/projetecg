function [ output ] = parasite( input,rayon )
%PARASITE Summary of this function goes here
%   Detailed explanation goes here*
M=input;
k=1:length(M)-rayon;
for i=1:rayon
    M(M(k+i)==1)=0;
end

k=(rayon+1):length(M);

for i=rayon:1
    M(M(k-i)==1)=0;
end

output =M;
end

