clear all
clc
close all
addpath('1_preTraitement','2_traitement','3_frequence');

rayon = 50;
filename='data/p_5AS3ExportDataECG.csv';
[t,x,y] = importfile(filename, 1,100000);

plot(t,x);

x=drift_Removal(x);
x=smoothing(x);

hold on
plot(t,x,'r');% x1= correl(x1);
legend('x','x-detrend');

%% detection de pics
f=facteur(filename);
detect_pics=seuillage(f*x);

figure(2);
plot(t,x,'r');
hold on
stem(t,detect_pics,'b');

%% Calcul frequence 
freq=frequence(detect_pics);
figure(3);
plot(freq,'r');
freq=smoothing(freq);
hold on 
plot(freq,'b');
grid

