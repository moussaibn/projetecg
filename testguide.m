function varargout = testguide(varargin)
% TESTGUIDE MATLAB code for testguide.fig
%      TESTGUIDE, by itself, creates a new TESTGUIDE or raises the existing
%      singleton*.
%
%      H = TESTGUIDE returns the handle to a new TESTGUIDE or the handle to
%      the existing singleton*.
%
%      TESTGUIDE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTGUIDE.M with the given input arguments.
%
%      TESTGUIDE('Property','Value',...) creates a new TESTGUIDE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before testguide_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to testguide_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help testguide

% Last Modified by GUIDE v2.5 02-Jun-2019 21:36:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @testguide_OpeningFcn, ...
                   'gui_OutputFcn',  @testguide_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before testguide is made visible.
function testguide_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to testguide (see VARARGIN)

set(gcf, 'Position', get(0, 'Screensize'));
clc
% Choose default command line output for testguide
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes testguide wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = testguide_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
contents = cellstr(get(hObject,'String'));
choice =  contents{get(hObject,'Value')} ;
if(strcmp(choice,'File 1'))
    filename = 'data/p1_AS3ExportDataECG.csv';

elseif(strcmp(choice,'File 2'))
    filename = 'data/p2_AS3ExportDataECG.csv';

elseif(strcmp(choice,'File 3'))
    filename = 'data/p3_AS3ExportDataECG.csv';
elseif(strcmp(choice,'File 4'))
    filename = 'data/p4_AS3ExportDataECG.csv';
elseif(strcmp(choice,'File 5'))
    filename = 'data/p_5AS3ExportDataECG.csv';

end
%Sauvegarde de ma variable filename
handles.filename=filename;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonLoad.
function buttonLoad_Callback(hObject, eventdata, handles)
% hObject    handle to buttonLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filename=handles.filename;
startRow = handles.start;
endRow=handles.end;
[t,x,y] = importfile(filename, startRow, endRow);
axes(handles.axesRaw);
comet(x);
xlabel('Samples');
ylabel('Voltage (mV)');
legend('Origninal ECG');
grid on;

handles.rawx =x;
handles.rawy=y;
handles.rawt=t;
guidata(hObject, handles);

% --- Executes on button press in buttonDriftRemoval.
function buttonDriftRemoval_Callback(hObject, eventdata, handles)
% hObject    handle to buttonDriftRemoval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xd=drift_Removal(handles.rawx);
axes(handles.axesSignal);
plot(xd);
xlabel('Samples');
ylabel('Voltage (mV)');
legend('Signal with Removed Baseline Drift');
grid on;
handles.xdrifted=xd;
guidata(hObject, handles);


% --- Executes on button press in buttonDenoise.
function buttonDenoise_Callback(hObject, eventdata, handles)
% hObject    handle to buttonDenoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xn=smoothing(handles.xdrifted);
axes(handles.axesSignal);
plot(xn);
xlabel('Samples');
ylabel('Voltage (mV)');
legend('Signal denoised');
grid on;
hold on
handles.xdenoised=xn;
guidata(hObject, handles);

% --- Executes on button press in buttonRWaves.
function buttonRWaves_Callback(hObject, eventdata, handles)
% hObject    handle to buttonRWaves (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
locs_Rwave= r_waves(handles.xdenoised);

axes(handles.axesSignal);
plot(locs_Rwave,handles.xdenoised(locs_Rwave),'rv','MarkerFaceColor','r');
xlabel('Samples');
ylabel('Voltage (mV)');
legend('ECG','R-Waves');
grid on;
handles.rtimes=locs_Rwave/300;
guidata(hObject, handles);

% --- Executes on button press in buttonSWaves.
function buttonSWaves_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSWaves (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
locs_Swave= s_waves(handles.xdrifted);

axes(handles.axesSignal);
plot(locs_Swave,handles.xdenoised(locs_Swave),'rs','MarkerFaceColor','b')
xlabel('Samples');
ylabel('Voltage (mV)');
legend('ECG','S-Waves');
grid on;



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editEnd_Callback(hObject, eventdata, handles)
% hObject    handle to editEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEnd as text
%        str2double(get(hObject,'String')) returns contents of editEnd as a double
handles.end=str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonLoad.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to buttonLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function editStart_Callback(hObject, eventdata, handles)
% hObject    handle to editStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStart as text
%        str2double(get(hObject,'String')) returns contents of editStart as a double
handles.start=str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonClearAxes.
function buttonClearAxes_Callback(hObject, eventdata, handles)
% hObject    handle to buttonClearAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 cla(handles.axesRaw,'reset');
 cla(handles.axesSignal,'reset');
 

% --- Executes on button press in buttonInterval.
function buttonInterval_Callback(hObject, eventdata, handles)
% hObject    handle to buttonInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
rtimes=handles.rtimes;
for i=2:length(rtimes)
    rdiff=rtimes(i)-rtimes(i-1);
end
set(handles.textInterval,'String',mean(rdiff));
handles.interval=mean(rdiff);
guidata(hObject,handles);

% --- Executes on button press in buttonBpm.
function buttonBpm_Callback(hObject, eventdata, handles)
% hObject    handle to buttonBpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpm=60/handles.interval;
set(handles.textBpm,'String',bpm);
handles.bpm=bpm;
guidata(hObject,handles);


% --- Executes on button press in buttonAnalysis.
function buttonAnalysis_Callback(hObject, eventdata, handles)
% hObject    handle to buttonAnalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpm=handles.bpm;
if(bpm<=100 && bpm>=50)
    set(handles.textAnalysis,'String','Normal');
    set(handles.textAnalysis,'ForegroundColor','green');
elseif (bpm<50)
    set(handles.textAnalysis,'String','Bradycardia');
    set(handles.textAnalysis,'ForegroundColor','red');
  elseif (bpm>100)
    set(handles.textAnalysis,'String','Tachycardia');
    set(handles.textAnalysis,'ForegroundColor','red');  
end

% --- Executes during object creation, after setting all properties.
function textInterval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in buttonSave.
function buttonSave_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
